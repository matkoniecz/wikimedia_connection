likely should not be used if you can avoid it

nowadays I would write something far better than THIS

it has several horrible design decisions, starting from storing cache elements as files, not in the database

also, you need to setup [osm_handling_config](https://codeberg.org/matkoniecz/osm_handling_config) first (pull request to remove that dependency is highly welcome here!)

# Running tests

`python3 -m unittest`
