#!/bin/bash
set -Eeuo pipefail
IFS=$'\\n\\t'
err_report() {
    echo "Error on line $1"
}
trap 'err_report $LINENO' ERR

rm dist -rf
python3 setup.py sdist bdist_wheel

# better as library?
python3 ../../python_package_reinstaller/reinstaller.py wikimedia_connection

python3 -m unittest
# twine upload dist/* # to upload to PyPi
