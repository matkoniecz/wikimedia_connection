import unittest
import wikimedia_connection.wikimedia_connection as wikimedia_connection
import osm_handling_config.global_config as osm_handling_config
import wikimedia_connection.wikidata_processing as wikidata_processing
from io import BytesIO

class Tests(unittest.TestCase):
    def test_empty(self):
        pass

    def test_excluding(self):
        # https://www.wikidata.org/wiki/Q2309609
        wikimedia_connection.set_cache_location(osm_handling_config.get_wikimedia_connection_cache_location())
        assert "Q1370598" not in wikidata_processing.get_useful_direct_parents("Q2309609", []), 'deprecated values are skipped'
        assert "Q392371" not in wikidata_processing.get_useful_direct_parents("Q2309609", []), 'deprecated values are skipped'
 
    def test_location_data(self):
        self.assertNotEqual(wikimedia_connection.get_location_from_wikidata('Q250645'), None)



    def test_old_style_be_tarask_link_language_code(self):
        # https://be-tarask.wikipedia.org/wiki/Цярэшкі%20(Шаркоўшчынскі%20раён)
        # https://www.wikidata.org/wiki/Q6545847
        self.assertEqual("be-tarask", wikimedia_connection.get_language_code_from_link("be-tarask:Цярэшкі (Шаркоўшчынскі раён)"))

    def test_old_style_be_tarask_split_link_wikidata_extraction(self):
        self.assertEqual("Q6545847", wikimedia_connection.get_wikidata_object_id_from_article("be-tarask", "Цярэшкі (Шаркоўшчынскі раён)"))

    def test_old_style_be_tarask_split_link_wikidata_data_extraction(self):
        self.assertEqual("Q6545847", list(wikimedia_connection.get_data_from_wikidata("be-tarask", "Цярэшкі (Шаркоўшчынскі раён)", forced_refresh=True)['entities'].keys())[0])

    def test_old_style_be_tarask_link_wikidata_extraction(self):
        # https://be-tarask.wikipedia.org/wiki/Цярэшкі%20(Шаркоўшчынскі%20раён)
        # https://www.wikidata.org/wiki/Q6545847
        self.assertEqual("Q6545847", wikimedia_connection.get_wikidata_object_id_from_link("be-tarask:Цярэшкі (Шаркоўшчынскі раён)"))


if __name__ == '__main__':
    unittest.main()
